/**
 * The Collection of Combat entities
 */
declare class CombatEncounters extends Collection {
	// @TODO: Declare
}

/**
 * The Combat Entity defines a particular combat encounter which can occur within the game session
 * Combat instances belong to the CombatEncounters collection
 */
declare class Combat extends Entity {
	// @TODO: Declare
}