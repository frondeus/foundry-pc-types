declare interface String {
	capitalize(): string;

	titleCase(): string;
}